﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HazaraBestPractice
{
    class Program
    {

        #region Ticket POCO
        interface IPoco { }
        class Ticket : IPoco
        {
            public Ticket()
            {

            }
            public Int64 Id { get; set; }
            public string MovieName { get; set; }
            public DateTime TimeStamp { get; set; }
            public double Price { get; set; }
            public int Hall { get; set; }

            public override bool Equals(object obj)
            {
                return this.Id == ((Ticket)obj).Id;
            }

            public override int GetHashCode()
            {
                return (int)Id;
            }
            public static bool operator ==(Ticket ticket1, Ticket ticket2)
            {
                return ticket1.Id == ticket2.Id;
            }
            public static bool operator !=(Ticket ticket1, Ticket ticket2)
            {
                return !(ticket1 == ticket2);
            }
            public override string ToString()
            {
                return $"{Newtonsoft.Json.JsonConvert.SerializeObject(this)}";
            }

        }
        #endregion

        private static readonly log4net.ILog my_logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        static List<Ticket> GetAllTickets(SqlConnection connection)
        {
            string query = "SELECT * FROM TICKETS";
            try
            {
                SqlCommand cmd = new SqlCommand(query, connection);
                cmd.CommandType = CommandType.Text;

                SqlDataReader reader = cmd.ExecuteReader(CommandBehavior.Default);

                List<Ticket> list = new List<Ticket>();

                while (reader.Read() == true)
                {
                    list.Add(
                        new Ticket
                        {
                            Id = Convert.ToInt64(reader["Id"]),
                            MovieName = reader["MOVIENAME"].ToString(),
                            TimeStamp = Convert.ToDateTime(reader["TIMESTAMP"]),
                            Price = Convert.ToDouble(reader["PRICE"]),
                            Hall = Convert.ToInt32(reader["HALL"])
                        });
                }

                return list;
            }
            catch (Exception ex)
            {
                my_logger.Error($"Failed to get tickets from DB [SELECT * FROM TICKETS]. Error : {ex}");
                return null;
            }
        }

        static List<Ticket> Sp_Get_Tickets(SqlConnection connection)
        {
            string sp_name = "GET_TICKETS";
            try
            {
                SqlCommand cmd = new SqlCommand(sp_name, connection);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlDataReader reader = cmd.ExecuteReader(CommandBehavior.Default);

                List<Ticket> list = new List<Ticket>();

                while (reader.Read() == true)
                {
                    list.Add(
                        new Ticket
                        {
                            Id = Convert.ToInt64(reader["Id"]),
                            MovieName = reader["MOVIENAME"].ToString(),
                            TimeStamp = Convert.ToDateTime(reader["TIMESTAMP"]),
                            Price = Convert.ToDouble(reader["PRICE"]),
                            Hall = Convert.ToInt32(reader["HALL"])
                        });
                }

                return list;
            }
            catch (Exception ex)
            {
                my_logger.Error($"Failed to run sp from DB [GET_TICKETS]. Error : {ex}");
                return null;
            }
        }

        static List<Ticket> SP_SELECT_MOVIE_BY_HALL(SqlConnection connection, int hall)
        {
            string sp_name = "SELECT_MOVIE_BY_HALL";
            try
            {
                SqlCommand cmd = new SqlCommand(sp_name, connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@hall", hall);

                SqlDataReader reader = cmd.ExecuteReader(CommandBehavior.Default);

                List<Ticket> list = new List<Ticket>();

                while (reader.Read() == true)
                {
                    list.Add(
                        new Ticket
                        {
                            Id = Convert.ToInt64(reader["Id"]),
                            MovieName = reader["MOVIENAME"].ToString(),
                            TimeStamp = Convert.ToDateTime(reader["TIMESTAMP"]),
                            Price = Convert.ToDouble(reader["PRICE"]),
                            Hall = Convert.ToInt32(reader["HALL"])
                        });
                }

                return list;
            }
            catch (Exception ex)
            {
                my_logger.Error($"Failed to run sp from DB [sp_name] parameter: {hall}. Error : {ex}");
                return null;
            }
        }

        private static SqlConnection GetOpenConnection(string conn)
        {
            SqlConnection result = new SqlConnection(conn);
            result.Open();
            return result;
        }

        static void Main(string[] args)
        {
            my_logger.Info("******************** System startup");

            //string m_conn = "Data Source=.;Initial Catalog=Many;Integrated Security=True";
            //float ver = 2.5f;

            string m_conn = HazaBestPracticeAppConfig.Instance.ConnectionString;
            float ver = HazaBestPracticeAppConfig.Instance.Version;
            string app_name = HazaBestPracticeAppConfig.Instance.AppName;

            Console.WriteLine($"Welcome to {app_name} ver {ver}");

            //List<Ticket> tickets = GetAllTickets(GetOpenConnection(m_conn));

            // sp Get tickets
            Console.WriteLine(" ********* sp GET_TICKETS");
            List<Ticket> tickets = Sp_Get_Tickets(GetOpenConnection(m_conn));
            tickets.ForEach(_ => Console.WriteLine(_));

            // sp SELECT_MOVIE_BY_HALL 2
            Console.WriteLine(" ********* sp SELECT_MOVIE_BY_HALL by hall 1");
            List <Ticket> tickets_by_hall = SP_SELECT_MOVIE_BY_HALL(GetOpenConnection(m_conn), 2);
            tickets_by_hall.ForEach(_ => Console.WriteLine(_));

            // sp SELECT_MOVIE_BY_HALL 1
            Console.WriteLine(" ********* sp SELECT_MOVIE_BY_HALL by hall user-choice");
            Console.WriteLine("Please enter hall ");
            int hall = Convert.ToInt32(Console.ReadLine());
            tickets_by_hall = SP_SELECT_MOVIE_BY_HALL(GetOpenConnection(m_conn), hall);
            tickets_by_hall.ForEach(_ => Console.WriteLine(_));

            my_logger.Info("******************** System shutdown");

            Console.ReadLine();

        }
    }
}
